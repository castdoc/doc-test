---
title: "Quickstart"
linkTitle: "Quickstart"
description: "Simple instructions to get you up and running with CAST Imaging 3.x on a single Microsoft Windows machine"
type: "docs"
weight: 5
no_list: true
---

## Overview
Instructions for installing CAST Imaging 3.x on one single Microsoft Windows machine: this quickstart guide covers the installation and initial configuration of the required components, a first basic analysis and information about accessing and consuming your results.

## Prerequisites
- Physical or virtual machine
- 64bit Windows 10/11, Windows Server 2016/2019/2022
- 16GB RAM minimum (32GB RAM recommended)
- 256GB SSD minimum
- Java JRE (≥ 17 LTS 64bit), with appropriate %JAVA_HOME% system environment variable
- Machine with fixed IP address / hostname recommended
- A valid global license key
- A valid [CAST Extend API key](https://extend.castsoftware.com/#/profile/settings)
- The following TCP ports should be unused:
    - CAST Storage Service:
        - 2284
    - CAST Imaging Console / CAST Imaging Node services:
        - 8090: Imaging Gateway Service
        - 8091: Imaging Console Service
        - 8092: Authentication service
        - 8096: SSO Service
        - 8098: Imaging Control Panel
        - 8099: Analysis Node
        - 8190: Analysis Node Log Service
    - CAST Imaging Viewer services:
        - 6362: Neo4j
        - 7473: Neo4j
        - 7474: Neo4j
        - 7687: Neo4j
        - 8083: Frontend
        - 9000: imaging-service
        - 9001: imaging-etl
        - 9980: sourcecode service

> Ensure that no existing installations of CAST Console/Nodes (v1/v2/v3) or CAST Imaging UI (v1/v2/v3) are present.

## Install CAST Storage Service
Download the latest release of [com.castsoftware.css](https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=latest) and unzip it anywhere on your local disk. To run the installer, execute the `setup.bat` file with elevated permissions (right click, `Run as administrator`) at the root of the unzipped files. The install process will then proceed. CAST recommends leaving all settings at their default position.

A successful installation will result in a functioning CAST Storage Service on port `2284` with `operator/CastAIP` default credentials, used to store analysis results and persistence data for your CAST Imaging installation.

## Install CAST Imaging Core
Download the latest release of [com.castsoftware.aip](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip&version=latest) and unzip it anywhere on your local disk. To run the installer, execute the `setup.bat` file with elevated permissions (right click, `Run as administrator`) at the root of the unzipped files. The install process will then proceed. CAST recommends leaving all settings at their default position.

A successful installation will result in a functioning CAST Imaging Core - the analysis "engine" for your CAST Imaging installation. 

## Install CAST Imaging Console and CAST Imaging Node service components
Download the latest release of the [com.castsoftware.imaging.console](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.console&version=latest) installer and unzip it anywhere on your local disk. Locate the `config_installer.conf` file at the root of the unzipped files and open with a text editor. Modify this according to your installation requirements:

- `UNINSTALL`: whether you want the script to fully uninstall a previous installation before proceeding.
- `HOST_HOSTNAME`: configure the machine's FQDN (fully qualified domain name) or its static IP address, e.g. `imaging.corp.domain.com` (CAST recommends avoiding the use of `localhost` where possible).
- `CSS_INFOS`: details of your local CAST Storage Service/PostgreSQL instance in the format `HOST_NAME:PORT`, e.g. `imaging.corp.domain.com:2284` (CAST recommends avoiding the use of `localhost` where possible).
- `CSS_USER` and `CSS_PASSWORD`: credentials for your local CAST Storage Service/PostgreSQL instance installed previously (username/password) - by default these will be `operator/CastAIP`.
- `V3_DB_NAME`: the database you would like to use for the `admin-center` and `aip-node` persistence schemas (leave at the default `postgres`).
- `INSTALL_DIR`: root installation location for this installer, e.g. `C:\CAST\install` or `C:\Program Files\CAST\install`. If the chosen path is anything other than`C:\Program Files`, it must not contain any white space.
- `AIP_INSTALLDIR`: Installation location of CAST Imaging Core component installed previously, e.g: `C:\Program Files\CAST\8.3`.

To run the installer, open a CMD window with elevated permissions (right click, `Run as administrator`) and execute the following command from the root of the unzipped files:

```text
imaging-v3-installer.bat configFile=config_installer.conf
```
The install process will then proceed: a successful installation will result in a functioning CAST Imaging Console and a CAST Node service declared in CAST Imaging Console.

> - A database called `keycloak_v3` will be created on the target CAST Storage Service/PostgreSQL instance for storing authentication data - if this database already exists it will be re-used and this may cause unexpected behaviour.
> - Persistence schemas called `admin_center` and `analysis_node` will be created in the database of your choice on the target CAST Storage Service/PostgreSQL instance - if these schemas already exist they will be re-used and this may cause unexpected behaviour.


## Install CAST Imaging Viewer

Download the latest release of the [com.castsoftware.imaging.viewer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.viewer&version=latest) installer and unzip it anywhere on your local disk. To run the installer, execute the `ImagingSystemSetup.exe` file at the root of the unzipped files.

Ensure that you configure the following options in the installer to point to your CAST Imaging Console installation (you can leave all other fields at their default):

- `Eureka host`: the FQDN or IP address of the machine on which the CAST Console component has been installed, e.g. `imaging.corp.domain.com` (CAST recommends avoiding the use of `localhost` where possible)
- `Eureka port`: the `Imaging Control Panel` service port number, `8098` by default.

![CAST Imaging Viewer installer](images/imaging_setup.jpg)

The install process will then proceed: a successful installation will result in a functioning CAST Imaging Viewer component declared in CAST Imaging Console.

## Initial configuration

The CAST Imaging Console component will be opened in your default browser (alternatively browse to `http://HOST_HOSTNAME:8090`). Login using the default `admin/admin` credentials. You will be prompted to configure:

- the global license key:

![License key](images/license_key.jpg)

- CAST Extend settings (API key/proxy):

![CAST Extend settings](images/extend.jpg)

As a final check, browse to the URL below and ensure that you have at least one node and the CAST Imaging Viewer components listed:
```
http://HOST_HOSTNAME:8090/admin/services
```
![Services](images/services.jpg)

## Onboard a simple application

### Prepare your source code

Prepare a ZIP file containing the source code you would like to onboard and analyze:

- create a temporary folder
- add your client code in one sub-folder
- add your database code in another sub-folder
- zip the two sub-folders into one single ZIP file

For example:

```
D:\temp
    |---JEE
    |---SQL
```

### Onboard your new application

Browse to `http://HOST_HOSTNAME:8090/home/applications`, click `Onboard application` and then enter a name for your application (1) and click the upload button (2) to attach the ZIP file you created previously:

![Onboard app](images/onboard_app.jpg)

Now click `Run Scan` (1) to begin the initial "fast scan" of your source code: this allows CAST to discover what is in the ZIP file and how to handle it during the analysis process:

![Run scan](images/onboard_app2.jpg)

### Start your analysis

When the ZIP file has been scanned, results are automatically displayed. Scroll to the bottom of the page and check the panel at the bottom: if you have a green tick (1), you are good to go and you can start the analysis process (2):

![Alt text](images/green_tick.jpg)

> If you do not have a green tick, you may need to re-organize the source code in your ZIP file.

### Consult your results

When the analysis is complete, browse to `http://HOST_HOSTNAME:8090/home/applications`, and click `Ready to view` for your application:

![Ready to view](images/ready_to_view.jpg)

The results of your application analysis will be displayed in CAST Imaging Viewer. Use the left hand panel, or double click items in the "view" to navigate your application and the objects within it:

![Results](images/results.jpg)
