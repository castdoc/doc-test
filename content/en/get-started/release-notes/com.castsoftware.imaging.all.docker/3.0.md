---
title: "3.0"
linkTitle: "3.0"
---

## 3.0.0-beta5

### Note

Note that an in-place upgrade from previous 3.0.0 betas is not possible. Please uninstall all existing 3.0.0 beta deployments before installing 3.0.0-beta5. No updates have been applied to the Docker installer in this release.
## 3.0.0-beta4

### Note

Note that an in-place upgrade from previous 3.0.0 betas is not possible. Please uninstall all existing 3.0.0 beta deployments before installing 3.0.0-beta4. No updates have been applied to the Docker installer in this release.
## 3.0.0-beta3

### Note

Note that an in-place upgrade from previous 3.0.0 betas is not possible. Please uninstall all existing 3.0.0 beta deployments before installing 3.0.0-beta3. No updates have been applied to the Docker installer in this release.
## 3.0.0-beta2

### Note

Note that an in-place upgrade from 3.0.0-beta1 to 3.0.0-beta2 is not possible. Please uninstall all existing 3.0.0-beta1 deployments before installing 3.0.0-beta2.
### New Features

| Summary | Details |
| ------- | ------- |
| UI - Admin Center - Upload CAST Imaging Viewer application ZIPs | A new menu option "Upload result into Imaging Viewer" has been added in the Admin Center > Applications menu allowing Admin users to upload CAST Imaging Viewer application ZIPs (extracted using the exporter.exe tool). Doing so will make the application ready to view, but the application cannot be scanned or analyzed. |

### Feature Improvements

| Summary | Details |
| ------- | ------- |
| UI - Onboarding | It is now possible to onboard application source code from a source folder location (previously only ZIP uploads were possible). |

## 3.0.0-beta1

### Note

This extension provides an installer via "docker-compose" for the installation of the CAST Console and CAST Viewer components on a single server where Docker is already installed. A UI installer is provided for the CAST Node service for installation on a Microsoft Windows server. See https://castdoc.gitlab.io/doc/docs/install/ for more information.
### Known Issues

| Internal Id | Details |
| ----------- | ------- |
| WEBI-15851 | Analysis data storage is currently stored in C:\\aip-node-data on the server on which the CAST Console component has been installed. It is not possible to change this location. |
| WEBI-00000 | Encrypted connections to your CAST Storage Service/PostgreSQL instance are not currently supported. |
| IMAGSYS-11639 | The automatic configuration of object source code display in CAST Viewer does not function, therefore source code is not available. |
| IMAGSYS-11899 | The App to App Dependencies Viewer is not available. |
| WEBI-15800 | When no CAST Nodes have been declared in CAST Console, or all CAST Nodes that have been declared are not functioning, then CAST Console does not display the "Services" menu in the Admin Center, thereby preventing you from viewing the status of any service. |
| WEBI-15838 | The Transactions > Rules > Check All Content option in the Application panel in CAST Console displays a blank page. |
| IMAGSYS-00000 | To define an OpenAI or Azure OpenAI API key, you will need to paste the following into the browser address bar "/imaging/default/admin/license", e.g. "http://host_hostname:port/imaging/default/admin/license". |
