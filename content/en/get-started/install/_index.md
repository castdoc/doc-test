---
title: "Installation guide for CAST Imaging"
linkTitle: "Installation"
description: "Advanced installation instructons for CAST Imaging 3.x"
type: "docs"
weight: 10
no_list: true
---

{{< cardpane >}}
{{% card header="**Microsoft Windows**" title="Direct installation on your Microsoft Windows machine:" footer="See [Installation on Microsoft Windows](windows)." %}}

This install option combines the following for direct installation on a single Microsoft Windows machine, or on two separate machines:

- a batch script for the CAST Console and CAST Node service components on one single machine
- a UI installer for the CAST Viewer component (on the same machine as the CAST Console component or on a separate machine)

 The CAST Core and CAST Storage Service/PostgreSQL instance components are not included but are required to complete the deployment.Additional CAST Storage Services/PostgreSQL instances can also be declared post install.
{{% /card %}}

{{% card header="**Docker**" title="Installation on Docker via docker-compose:" footer="See [Installation on Docker](docker)." %}}

This install option combines the following for installation on Docker on a single machine, or on multiple separate machines:

- the CAST Console and CAST Viewer components via `docker-compose` (all on one machine, or on two separate machines)
- a UI installer for the CAST Node service for direct installation on Microsoft Windows (additional Nodes can be declared post install)

The CAST Core component is not included, but is required for each CAST Node. The CAST Storage Service/PostgreSQL instance component is included as a Docker image and will be used, however additional remote CAST Storage Services/PostgreSQL instances can also be declared post install.
{{% /card %}}
{{< /cardpane >}}
