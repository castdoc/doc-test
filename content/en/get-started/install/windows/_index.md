---
title: "Installation on Microsoft Windows"
linkTitle: Microsoft Windows
type: "docs"
weight: 20
---

## Overview
This install option combines the following for direct installation on a single Microsoft Windows server, or on two separate servers:

- [com.castsoftware.imaging.console](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.console&version=latest): a batch script for the CAST Console and CAST Node service components on one single server
- [com.castsoftware.imaging.viewer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.viewer&version=latest): a UI installer for the CAST Viewer component (on the same server as the CAST Console component or on a separate server)

 The [CAST Core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip&version=latest) and [CAST Storage Service/PostgreSQL instance](https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=latest) components are not included but are required to complete the deployment. Additional CAST Storage Services/PostgreSQL instances can also be declared post install.

{{% alert title="Note"%}}
Please ensure that no existing installations of CAST Console/Nodes (v1 or v2) or CAST Imaging UI (v1 or v2) are present.
{{% /alert %}}

## Prerequisites

- Physical or virtual server
- 64bit Windows 10/11, Windows Server 2016/2019/2022
- 16GB RAM minimum (32GB RAM recommended)
- 256GB SSD
- JDK (≥ 17 LTS 64bit), with appropriate %JAVA_HOME% system environment variable
- Servers with fixed IP address / hostname recommended
- [CAST Core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip&version=latest) installed on the server where CAST Console will reside (latest release)
- Access to a local or remote [CAST Storage Service/PostgreSQL instance](https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=latest)
    - A database called `keycloak_v3` will be created on the target CAST Storage Service/PostgreSQL instance - if this database already exists it will be re-used and this may cause unexpected behaviour
    - Schemas called `admin_center` and `aip_node` will be created in the database of your choice on the target CAST Storage Service/PostgreSQL instance - if these schemas already exist they will be re-used and this may cause unexpected behaviour
- Local administrator privileges (ability to `Run as administrator`)
- A valid global license key
- A valid [CAST Extend API key](https://extend.castsoftware.com/#/profile/settings)
- The following TCP ports should be unused:

```text
CAST Console / CAST Node:

8090: Imaging Gateway Service
8091: Imaging Console Service
8092: Authentication service
8096: SSO Service
8098: Imaging Control Panel
8099: Analysis Node
8190: Analysis Node Log Service

CAST Viewer:

6362: Neo4j
7473: Neo4j
7474: Neo4j
7687: Neo4j
8083: Frontend
9000: imaging-service
9001: imaging-etl
9980: sourcecode service
```

## Install CAST Console and CAST Node service components

Download the latest release of the installer and unzip it anywhere on your local disk. Locate the `config_installer.conf` file at the root of the unzipped files and open with a text editor. Modify this according to your installation requirements:

- `UNINSTALL`: whether you want to the script to fully uninstall a previous installation before proceeding.
- `HOST_HOSTNAME`: configure the server’s FQDN (fully qualified domain name) or its static IP address, e.g. `imaging.corp.domain.com`.
- `CSS_INFOS`: details of your local or remote CAST Storage Service/PostgreSQL instance in the format `HOST_NAME:PORT`, e.g. `css.corp.domain.com:2284`.
- `CSS_USER` and `CSS_PASSWORD`: credentials for your CAST Storage Service/PostgreSQL instance (username/password).
- `V3_DB_NAME`: the database you would like to use for the `admin-center` and `aip-node` schemas (`postgres` or other custom database).
- `INSTALL_DIR`: root installation location for this installer, e.g. `C:\CAST\install` or `C:\Program Files\CAST\install`. If the chosen path is anything other than`C:\Program Files`, it must not contain any white space.
- `AIP_INSTALLDIR`: CAST Core component installation location (i.e. where CAST Core is installed) e.g: `C:\Program Files\CAST\8.3`.

To run the installer, open a CMD window with elevated permissions (right click, `Run as administrator`) and execute the following command from the root of the unzipped files:

```text
imaging-v3-installer.bat configFile=config_installer.conf
```
The install process will then proceed: a successful installation will result in a functioning CAST Console and a CAST Node declared in CAST Console.

## Install CAST Viewer component

The CAST Viewer component can be installed on the same server as the CAST Console / CAST Node components or on a separate dedicated server. Download the latest release of the installer and unzip it anywhere on your local disk. To run the installer, double click the `ImagingSystemSetup.exe` file at the root of the unzipped files. The installer is UI based and is straightforward.

Ensure that you configure the following options in the installer to point to your CAST Console installation (you can leave all other fields at their default):

- `Eureka host`: the FQDN or IP address of the server on which the CAST Console component has been installed, e.g. `imaging.corp.domain.com`.
- `Eureka port`: the `Imaging Control Panel` port number, 8098 by default.

![Viewer installer](images/imaging_setup.jpg)

The install process will then proceed: a successful installation will result in a functioning CAST Viewer component declared in CAST Console.

{{% alert title="Install on separate servers"%}}
When installing CAST Viewer on a separate server, you will need to download the latest release of the installer on the second server and unzip it anywhere on your local disk.

Ensure you can access the server on which you installed the CAST Console component from this new server using the following URL:
```text
http://HOST_HOSTNAME:8098
```
Then proceed as described [above](#install-cast-viewer-component).
{{% /alert %}}

## Initial start up configuration

The CAST Console component will be opened in your default browser on the server it has been installed on (alternatively browse to `http://HOST_HOSTNAME:8090`). Login using the default `admin/admin` credentials. You will be prompted to configure:

- the global license key:

![License key](images/license_key.jpg)

- CAST Extend settings (API key/proxy):

![CAST Extend settings](images/extend.jpg)

As a final check, browse to the URL below and ensure that you have at least one CAST Node and the CAST Viewer components listed:
```
http://HOST_HOSTNAME:8090/console/#/aic/manage/nodes
```
![Services](../docker/images/services.jpg)

## What is installed?

The following Microsoft Windows services, set to start automatically and running with `LocalSystem`:

CAST Console / CAST Node:

- CAST Imaging Analysis Node
- CAST Imaging Analysis Node Log Service
- CAST Imaging Authentication
- CAST Imaging Console Service
- CAST Imaging Control Panel
- CAST Imaging Gateway Service
- CAST Imaging SSO Service

CAST Viewer:

- CAST Imaging - Frontend service
- CAST Imaging - imaging-etl
- CAST Imaging - imaging-service
- CAST Imaging - Neo4j Graph Database
- CAST Imaging - sourcecode service

## What about data/logs storage?

All analysis data will be stored by default in `C:\aip-node-data` on the server running CAST Console.

With regard to the CAST Console component, if you set `INSTALL_DIR` to a folder in `%PROGRAMFILES%`, data, logs and configuration files will be stored automatically in `%PROGRAMDATA%`. If you set a custom location, then data, logs and configuration files will be stored in a folder called `Data` in the root of `INSTALL_DIR`.

With regard to the CAST Viewer component, all data, logs and configuration files will be stored in the folders you defined during the installation.

## Uninstall process

### CAST Console and CAST Node service components
To run the uninstaller, open a CMD window with elevated permissions (right click, `Run as administrator`) and execute the following command from the root of the unzipped files:

```text
uninstall-cast-imaging-unified-v3.bat
```
This will remove all components and related Microsoft Windows Services, including the following database items (you will be prompted for details of the target CAST Storage Service/PostgreSQL instance):
- `keycloak_v3` database
- `admin_center` schema
- `aip_node` schema

### CAST Viewer and CAST Core components
Use the Microsoft Windows `Add or remove programs` feature to uninstall the components.

### What is not removed?
- Analysis data files in `C:\aip-node-data` on the server on which the CAST Console component has been installed. If you intend to perform a clean install you should ensure that these items are backed up and then removed.

## Known issues
See [com.castsoftware.imaging.console Release Notes](../../release-notes/com.castsoftware.imaging.console/) and [com.castsoftware.imaging.viewer Release Notes](../../release-notes/com.castsoftware.imaging.viewer/).
