---
title: "Installation on Docker"
linkTitle: "Docker"
type: "docs"
weight: 21
---

## Overview

This install option combines the following for installation on Docker on a single server, or on multiple separate servers:

- [com.castsoftware.imaging.all.docker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.all.docker&version=latestthe): CAST Console and CAST Viewer components via `docker-compose` (all on one server, or on two separate servers)
- a UI installer provided in [com.castsoftware.imaging.all.docker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.all.docker&version=latestthe) for the CAST Node service for direct installation on Microsoft Windows (additional Nodes can be declared post install)

The [CAST Core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip&version=latest) component is not included, but is required for each CAST Node. The [CAST Storage Service/PostgreSQL instance](https://extend.castsoftware.com/#/extension?id=com.castsoftware.css&version=latest) component is included as a Docker image and will be used, however additional remote CAST Storage Services/PostgreSQL instances can also be declared post install.

## Prerequisites

- Physical or virtual server
- 64bit Windows 10/11, Windows Server 2016/2019/2022, 64bit Linux (Red Hat or Debian derivative)
- 16GB RAM minimum (32GB RAM recommended)
- 256GB SSD
- JDK (≥ 17 LTS 64bit)
- Servers with fixed IP address / hostname recommended
- A Node service and [CAST Core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip&version=latest) are required on a Microsoft Windows server (local or remote)
- Local administrator/root privileges
- A valid global license key
- A valid [CAST Extend API key](https://extend.castsoftware.com/#/profile/settings)
- The following TCP ports should be available:

```text
PORT_IMAGING=8083
PORT_GATEWAY=8090
PORT_CONSOLE=8091
PORT_AUTH_SERVICE=8092
PORT_KEYCLOAK=8096
PORT_CONTROL_PANEL=8098
```

### For Docker on Linux

- `Docker` latest stable release
- `docker-compose` latest stable release

### For Docker on Microsoft Windows

- `Docker Desktop` for Microsoft Windows - latest stable release
- `Containers for Linux` must be enabled
- Hyper-V and WSL2 modes are both supported (WSL2 is recommended if remote access to CAST Imaging is required)

## Install CAST Console component

Download the latest release of the installer and unzip it anywhere on your local disk. Locate the `.env` file at the root of the unzipped files and open it in a text editor. Change only the following lines to:

- configure the server’s FQDN (fully qualified domain name) or its static IP address
- set the location of the three required folders: the paths need to point to a mapped network share drive or use a UNC path of the network share, for example: \\shared\console\delivery

```text
HOST_HOSTNAME=imaging.corp.domain.com
DELIVERY_FOLDER=\\shared\console\delivery
DEPLOY_FOLDER=\\shared\console\deploy
SHARED_FOLDER=\\shared\console\common-data
```

Now run the following command from the root of the unzipped files to pull the CAST Console component images from Docker Hub and start the containers:

```text
docker-compose up -d
```
Check the status of the various services - there should be *six* in total:

```text
docker ps
```
A successful installation will result in a functioning CAST Console.

## Install CAST Viewer component

Run the following command from the `imaging` folder which can be found at the root of the unzipped installation files. This will install the required files, pull the CAST Viewer component images from Docker Hub and start the containers:

{{< tabpane >}}
{{< tab header="Operating System" disabled=true />}}
{{< tab "Linux" >}}
./imagingsetup -ch imaging.corp.domain.com -cp 8098 -d /home/CAST/imaging  -i install
{{< /tab >}}
{{< tab "Microsoft Windows" >}}
imagingsetup -ch imaging.corp.domain.com -cp 8098 -d  C:\CAST\imaging -i install
{{< /tab >}}
{{< /tabpane >}}

Where:
- `-ch` is the FQDN or IP address of the current server as defined in `HOST_HOSTNAME` in the `.env` file
- `-cp` is the `PORT_CONTROL_PANEL` port number as defined in the `.env` file
- `-d` is the path on disk where you would like to store the CAST Viewer data and configuration files

Check the status of the various services - there should be *three* in total specific to the CAST Viewer component:

```text
docker ps
```
A a successful installation will result in a functioning CAST Viewer component declared in CAST Console.

{{% alert title="Install on separate servers"%}}
When installing each component on a separate server, you will need to download the latest release of the installer on the second server and unzip it anywhere on your local disk.

Ensure you can access the server on which you installed the CAST Console component from this new server using the following URL:
```text
http://HOST_HOSTNAME:8098
```
Then proceed as described [above](#install-cast-viewer-component).
{{% /alert %}}

## Install a CAST Node

{{% alert title="Note"%}}
Until at least one CAST Node is installed and declared in CAST Console, you cannot onboard applications for analysis. CAST Node components can currently only be installed on Microsoft Windows servers.
{{% /alert %}}

To install a Node, download two components on your target Microsoft Windows server (this can be a server you have installed the Console/Viewer components on if it is a Microsoft Windows server, or it can be separate dedicated server). Both installers are UI based and should be installed in the order listed below:

- [CAST Core](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip&version=latest) (latest release)
- Dedicated CAST Node service - this is provided in an executable JAR file called `AIP-Node-<version>.jar` located at the root of the [com.castsoftware.imaging.all.docker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.imaging.all.docker&version=latestthe) installer.

Double click the `AIP-Node-<version>.jar` to begin the installation and ensure that the highlighted fields in the image below are configured correctly:

- `CAIP location` refers to the CAST Core component installation on the local server
- `Config server URL` should point to `http://HOST_HOSTNAME:PORT_CONTROL_PANEL/config` using references from the `.env` file on the server on which the CAST Console component has been installed, e.g. `imaging.corp.domain.com:8098/config`.

![Node installer](image/node.jpg)

A successful installation will result in a CAST Node declared in CAST Console. Check the following location to ensure your new CAST Node is visible:
```
http://HOST_HOSTNAME:8090/console/#/aic/manage/nodes
```
{{% alert title="Note"%}}
You will need to allow incoming connections on TCP port `8099` (or the custom port if you changed it during the CAST Node service installation) from the server on which CAST Console has been installed.
{{% /alert %}}

## Initial start up configuration

When the install is complete, browse to the URL below and login using the default `admin/admin` credentials: 

```
http://HOST_HOSTNAME:8090
```

You will be prompted to configure:

- the global license key:

![License key](../windows/images/license_key.jpg)

- CAST Extend settings (API key/proxy):

![CAST Extend settings](../windows/images/extend.jpg)

As a final check, browse to the URL below and ensure that you have at least one CAST Node and the CAST Viewer components listed:
```
http://HOST_HOSTNAME:8090/console/#/aic/manage/nodes
```
![Services](images/services.jpg)

## What is installed?

The following Docker containers will be created and are set to start automatically:

CAST Console:

- admin-center-v3
- aip-console-v3
- aip-sso
- auth-service-v3
- gateway-v3
- unified-postgres

CAST Viewer:

- imagingservice-v3
- imagingetl-v3
- imagingneo4j-v3

On each Node, two Microsoft Windows services:
- CAST AIP Console Standalone Service
- CAST AIP Node Log Service

## What about data storage?

All analysis data will be stored in the folders you defined in the `.env` file.

## What happens if a container does not start?

If a container fails to start, run the following command to display the container logs - this will give an indication of what may have gone wrong:
```text
docker logs <container_name>
```

## Uninstall process

### CAST Console and CAST Viewer components
Run the following commands to stop both components:

{{< tabpane >}}
{{< tab header="Operating System" disabled=true />}}
{{< tab "Linux" >}}
#CAST Console: must be run from the same folder as the docker-compose.yml file. Stops containers and removes containers, networks, volumes, and images created by `up`
docker-compose down
#CAST Viewer: must be run from the CAST Viewer installation folder
./imaging -s stop
{{< /tab >}}
{{< tab "Microsoft Windows" >}}
#CAST Console: must be run from the same folder as the docker-compose.yml file. Stops containers and removes containers, networks, volumes, and images created by `up`
docker-compose down
#CAST Viewer: must be run from the CAST Viewer installation folder
imaging -s stop
{{< /tab >}}
{{< /tabpane >}}

Now run the following commands to completely remove all components:

{{% alert title="Warning" color="warning" %}}
Use these commands with care if you have items unrelated to CAST running in your Docker instance: they will remove everything from your Docker instance, including the CAST Storage Service/PostgreSQL instance.
{{% /alert %}}

```text
#Remove all unused local volumes. Unused local volumes are those which are not referenced by any containers
docker volume prune
#Remove all unused containers, networks, images (both dangling and unreferenced)
docker system prune -a 
```

Finally, delete the installer ZIP file the unzipped folder and any installation folders.

### CAST Node and CAST Core component
Use the Microsoft Windows `Add or remove programs` feature to uninstall the components.

### What is not removed?
- Analysis data files in `\\server\share`

If you intend to perform a clean install you should ensure that these items are removed first.

## Known issues
See [com.castsoftware.imaging.console Release Notes](../../release-notes/com.castsoftware.imaging.console/) and [com.castsoftware.imaging.viewer Release Notes](../../release-notes/com.castsoftware.imaging.viewer/).
