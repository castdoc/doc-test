---
title: "CAST Documentation"
description: "Deep insights into your application's inner workings..."
type: "home"
---

<link href="/pagefind/pagefind-ui.css" rel="stylesheet">
<script src="/pagefind/pagefind-ui.js"></script>
<div id="search"></div>
<script>
    window.addEventListener('DOMContentLoaded', (event) => {
        new PagefindUI({ element: "#search", showSubResults: true });
    });
</script>

<div class="td-content">
	<h3>Explore our v3 documentation...</h3>
</div>

{{< cardpanehome >}}
{{% card header="[Quickstart](../quickstart)" %}}
<i class="fa-solid fa-play"></i>
Simple instructions to get you up and running with CAST Imaging 3.x on a single Microsoft Windows machine.
{{% /card %}}
{{% card header="[Deploy](../deploy)" %}}
<i class="fa-solid fa-download"></i>
Detailed information about deploy CAST Imaging on the platform of your choice.
{{% /card %}}
{{% card header="[Administer](../administer)" %}}
<i class="fa-solid fa-gear"></i>
How to administer your CAST Imaging installation once it is up and running.<br><br>
_COMING SOON..._
{{% /card %}}
{{% cardhome header="[Upgrade](../upgrade)" %}}
<i class="fa-solid fa-arrow-up-from-bracket"></i>
Learn how to upgrade all aspects of your CAST Imaging installation.<br><br>
_COMING SOON..._
{{% /cardhome %}}
{{< /cardpanehome >}}

<div class="td-content">
	<h3>Reference</h3>
</div>

{{< cardpanehome >}}
{{% card header="[Interface Reference](../reference)" %}}
<i class="fa-solid fa-book"></i>
Learn about the graphical user interface for configuring an analysis and consuming your results.<br><br>
_COMING SOON..._
{{% /card %}}
{{% card header="[Covered Technologies](../technologies)" %}}
<i class="fa-solid fa-microchip"></i>
Reference information about extensions for supported CAST technologies.
{{% /card %}}
{{% card header="[Requirements](../requirements)" %}}
<i class="fa-solid fa-clipboard-list"></i>
System requirements for your CAST Imaging deployment.<br><br>
{{% /card %}}
{{% cardhome header="[Legal](../legal)" %}}
<i class="fa-solid fa-scale-balanced"></i>
Legal information: EOL information, open source tools provided in CAST Imaging etc.<br><br>
{{% /cardhome %}}
{{< /cardpanehome >}}
