---
title: "Release Notes Testing"
linkTitle: "Release Notes Testing"
no_list: false
---

## Overview

The goal of these tests is to find a method to automatically display release notes in CAST Documentation from a URL (the same method used in Confluence). The ideal method would allow us to pull the required content from a URL (i.e. stored in a remote git repo) at build time. This would avoid the need to store the release notes markdown files in the "doc" repo (which would increase the number of files we store).