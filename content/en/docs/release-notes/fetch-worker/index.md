---
title: "fetch-worker"
linkTitle: "fetch-worker"
no_list: true
---

## Overview

The contents of this page are pulled at build time from a .md file stored in a remote repository here: https://github.com/mstura/doc_test/blob/master/markdown/com.castsoftware.dotnet/1.5.md. This uses a custom shortcode I have created called `fetch-worker`:

```
{{/*
  fetch-worker
  Fetch and render remote content from a JSON proxy
*/}}
{{ $remote_url := (.Get 0) }}
{{ $remote_data := getJSON "https://hello-world-patient-snowflake-7358.infranotifications.workers.dev/fetch?url=" $remote_url }}
{{ with $remote_data }}
    {{ $remote_data.body | safeHTML }}
{{ end }}
```

The shortcode `fetch-worker` is similar to `fetch-remote-github` in that it fetches the content via the github API in JSON format (not in raw markdown format) during the doc build using the `data.getJSON` function. However, it also relies on a Cloudflare JavaScript worker, which does the heavy lifting and transforms the JSON into markdown:

https://dash.cloudflare.com/2c5fd4c05e252ac98d5e19662d60ba44/workers/services/edit/hello-world-patient-snowflake-7358/production

This is based on this blog post: https://quinncasey.com/hugo-remote-content-from-url/.

> `data.getJSON` is deprecated as of Hugo 0.123.0.

Conclusions:

- Advantage: Does not require that the markdown file is present in the `doc` or `extensions` repos.
- Disadvantage: It uses the `getJSON` function which is deprecated (bad!)
- Disadvantage: The TOC on the right hand side is NOT built correctly.

-------------

{{< fetch-worker "https://raw.githubusercontent.com/mstura/doc_test/master/markdown/com.castsoftware.dotnet/1.5.md" >}}

