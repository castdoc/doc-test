---
title: "fetch-remote"
linkTitle: "fetch-remote"
no_list: true
rnurl: "https://extend.castsoftware.com/api/delta/export/release-notes/format/markdown?id=com.castsoftware.dotnet&major=1&minor=5"
---

## Overview

Using `resources.GetRemote` function (see this thread: https://discourse.gohugo.io/t/is-there-a-way-to-embed-raw-github-url-in-hugo/39957/13) to fetch a raw .md file from a remote repo via a shortcode called `fetch-remote`:

```
{{ $url := $.Page.Params.rnurl }}
{{ with resources.GetRemote $url }}
  {{ with .Err }}
    {{ errorf "%s" . }}
  {{ else }}
{{ .Content }}
  {{ end }}
{{ else }}
  {{ errorf "Unable to get remote resource." }}
{{ end }}
```

Conclusion:

- Advantage: Does not require that the markdown file is present in the `doc` or `extensions` repos.
- Advantage: Uses `$.Page.Params` to get the URL of the .md file from the frontmatter
- ~~Disadvantage: The TOC on the right hand side is NOT built correctly (bad!).~~ NOW FIXED, see https://discourse.gohugo.io/t/trying-to-fetch-markdown-from-a-remote-repository-using-resources-getremote/49156.

-------------

{{% fetch-remote %}}