---
title: "fetch-remote-github-copy"
linkTitle: "fetch-remote-github-copy"
no_list: true
---

## Overview

The contents of this page are pulled at build time from a .md file stored in a remote repository here: https://github.com/mstura/doc_test/blob/master/markdown/com.castsoftware.dotnet/1.5.md. This uses a custom shortcode I have created called `fetch-remote-github-copy`:

```
{{ $path := .Get 0 }}
{{ $json := getJSON "https://api.github.com/repos/mstura/doc_test/contents/markdown" $path }}
{{ $json.content | base64Decode | markdownify }}
```

The shortcode is very similar to `fetch-remote-github` (it fetches the content via the github API in JSON format (not in raw markdown format)) but it has been adapted to remove the call to `"pandoc"`.

> `data.getJSON` is deprecated as of Hugo 0.123.0.

Conclusions:

- Advantage: Does not require that the markdown file is present in the `doc` or `extensions` repos.
- Advantage: The `"pandoc"` warning inherent in the `fetch-remote-github` is removed.
- Disadvantage: The TOC on the right hand side is NOT built correctly (bad!)
- Disadvantage: It uses the `getJSON` function which is deprecated (bad!)

-------------

{{% fetch-remote-github-copy "/com.castsoftware.dotnet/1.5.md" %}}