---
title: "fetch-include"
linkTitle: "fetch-include"
no_list: true
---

## Overview

The contents of this page are pulled at build time from a .md file stored in /static/rn/. This content is pulled in as a module from a remote repo at build time. This uses a custom shortcode called `fetch-include`:

```
{{ $p := site.GetPage (.Get 0) }}
{{ $p.RenderShortcodes }}
```

Conclusions:

- Advantage: The TOC on the right hand side is built correctly.
- Disadvantage: Requires that the markdown file is present in the `doc` repo (i.e. pulled in via a module import).

---------------

{{% fetch-include "/rn/com.castsoftware.dotnet/1.5.md" %}}