---
title: "fetch-remote-github"
linkTitle: "fetch-remote-github"
no_list: true
---

## Overview

The contents of this page are pulled at build time from a .md file stored in a remote repository here: https://github.com/mstura/doc_test/blob/master/markdown/com.castsoftware.dotnet/1.5.md. This uses a custom shortcode I have created called `fetch-remote-github`:

```
{{ $path := .Get 0 }}
{{ $json := getJSON "https://api.github.com/repos/mstura/doc_test/contents/markdown" $path }}
{{ $opts := dict "markup" "pandoc" }}
{{ $json.content | base64Decode | $.Page.RenderString $opts }}
```

The shortcode fetches the content via the github API in JSON format (not in raw markdown format) during the doc build using the `data.getJSON` function from Hugo (https://gohugo.io/functions/data/getjson/). See also https://gohugo.io/methods/page/renderstring/. The shortcode transforms the JSON output into markdown.

There is an equivalent shortcode called `fetch-remote-gitlab` which will fetch from the gitlab API in JSON format.

This is based on this question: https://discourse.gohugo.io/t/include-content-of-a-url/27357/6

> `data.getJSON` is deprecated as of Hugo 0.123.0.

Conclusions:

- Advantage: Does not require that the markdown file is present in the `doc` or `extensions` repos.
- Advantage: The TOC on the right hand side is built correctly.
- Disadvantage: It uses the `getJSON` function which is deprecated (bad!)
- Disadvantage: Scroll bars do not work in output HTML if the content has very long words in it that overflow the width of the column - generally very long words are only created when a URL is pasted in (these are usually very long), but this can be avoided by using md syntax for URLs i.e. square brackets then round brackets.
- Disadvantage: Requires `pandoc` tool (see shortcode) to transform the JSON into markdown. This tool cannot be loaded in the Cloudflare build environment, and I have not found a way to get around this (yet). The Cloudflare build ignores the pandoc command and the end result functions fine, but a warning is output in the build log. Therefore we would have this warning message for every page that uses this shortcode to display release notes (warning message is shown below). There may also be a performance impact while Cloudflare searches for `pandoc` and cannot find it. See also https://gohugo.io/content-management/formats/.

```
pandoc not found in $PATH: Please install.
                  Leaving pandoc content unrendered.
```                

Testing notes:

```
{{ $path := .Get 0 }}
{{ $json := getJSON "https://api.github.com/repos/mstura/doc_test/contents/markdown" $path }}
{{ $opts := dict "markup" "pandoc"}}
{{ $json.content | base64Decode | $.Page.RenderString $opts }}
```
The line starting `$opts` is the line that causes the warning in the build log. I have tried:

- removing the `$opts` line (line 3) and the `$opts` variable on the fourth line: this removes the warning message in the log (great!), but it causes the right hand TOC not to be generated (bad!) because Hugo does not correctly handle the content as markdown.
- removing the `"pandoc"` option: this causes the build to fail entirely (bad!)
- changing the `"pandoc"` option to `"goldmark"` (a built in renderer): this removes the warning message in the log (great!), but it causes the right hand TOC not to be generated (bad!)
- changing the `"pandoc"` option to `"markdown"` (a built in renderer): this removes the warning message in the log (great!), but it causes the right hand TOC not to be generated (bad!)

-------------

{{% fetch-remote-github "/com.castsoftware.dotnet/1.5.md" %}}