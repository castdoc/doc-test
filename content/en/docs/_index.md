---
title: "CAST Imaging Documentation"
linkTitle: "Home"
no_list: true
---

{{< cardpane >}}
{{% card header="[Get Started](install)" class="fa-solid fa-download" %}}
<i class="fa-solid fa-play"></i>
Get started with CAST Imaging, including the available options for installation and the prerequisites that are required, plus a host of other useful information.
{{% /card %}}
{{% card header="[Install](install)" class="fa-solid fa-download" %}}
<i class="fa-solid fa-download"></i>
Learn how to get up and running with CAST Imaging on the platform of your choice.
{{% /card %}}
{{% card header="[Configure](install)" %}}
<i class="fa-solid fa-gear"></i>
Configure CAST Imaging the way you want: determine extension strategy, setup authentication and roles etc, using the UI Admin Center and configuration files.
{{% /card %}}
{{< /cardpane >}}

{{< cardpane >}}
{{% card header="[Onboard](install)" %}}
<i class="fa-solid fa-spinner"></i>
Learn how to onboard your application, run a CAST analysis and generate results.
{{% /card %}}
{{% card header="[User Guides](install)" %}}
<i class="fa-solid fa-book"></i>
Learn about the graphical user interface for onboarding your application in CAST Console and consuming your analysis results in CAST Viewer.
{{% /card %}}
{{% card header="[Technology Extensions](../extensions)" %}}
<i class="fa-solid fa-microchip"></i>
Technology extensions to enhance your CAST analysis…
{{% /card %}}
{{< /cardpane >}}
