---
title: "Test9 - Fetch git hub json"
linkTitle: "Test9 - Fetch git hub json"
no_list: true
---

The contents of this page are pulled at build time from a .md file stored in a remote repo, via the JSON API. This uses a custom shortcode called fetch-remote-github.

{{% fetch-remote-github "/com.castsoftware.dotnet/1.5.md" %}}
