---
title: "Test6"
linkTitle: "Test6"
no_list: true
---

The contents of this page are dynamically loaded from extend.castsoftware.com when the page is loaded (not at build time). This uses JavaScript. The downside is that because the page contents are not "built" by Hugo, there is no page toc in the right side bar and no way to generate that.

<script src="https://cdn.jsdelivr.net/npm/marked@3.0.7/marked.min.js"></script>
<div id="release-notes"></div>

<script>
// Fetch remote markdown file
fetch('https://extend.castsoftware.com/api/delta/export/release-notes/format/markdown?id=com.castsoftware.aip.console&major=2&minor=11.md')
  .then(response => response.text())
  .then(markdown => {
	// Convert markdown to HTML
	const html = marked(markdown);

	// Display the HTML in the webpage
	const contentDiv = document.querySelector('#release-notes');
	contentDiv.innerHTML = html;
  });
</script>

