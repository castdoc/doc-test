---
title: "Test"
linkTitle: "Test"
no_list: true
---

The contents of this page are pulled at build time from a .md file stored in a remote repo (but can also be stored in the same repo). This uses a custom shortcode called fetch-remote. The content is fetched via the gitlab API in JSON format. Works well, except that horizontal scroll bars do not work for some reason if the content has very long words in it that overflow the width - generally very long words are only created when a URL is pasted in (these are usually very long), but this can be avoided by using md syntax for URLs i.e. square brackets then round brackets as in the links below.

{{% fetch-remote-gitlab "content%2fen%2frelease-notes%2f2.11.md?ref=main" %}}
