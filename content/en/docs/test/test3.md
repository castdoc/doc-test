---
title: "Test3"
linkTitle: "Test3"
no_list: true
---

The contents of this page are dynamically loaded from extend.castsoftware.com when the page is loaded (not at build time). This uses JavaScript. The downside is that because the page contents are not "built" by Hugo, there is no page toc in the right side bar and no way to generate that.

<script src="https://unpkg.com/markdown-it@8.4.2/dist/markdown-it.min.js"></script>
<script>
const md = window.markdownit({
  linkify: true
});

fetch('https://extend.castsoftware.com/api/delta/export/release-notes/format/markdown?id=com.castsoftware.aip.console&major=2&minor=11')
  .then((response) => response.text())
  .then((text) => {
    document.getElementById('output').innerHTML = md.render(text);
  })
</script>
<div id="output"></div>
