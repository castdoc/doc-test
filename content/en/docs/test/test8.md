---
title: "Emoji TEST"
linkTitle: "Emoji TEST"
no_list: true
---

:x:

:heavy_check_mark:

| Icon | Description    |
|------|----------------|
|  :x:    | Another thing  |
|  :heavy_check_mark:    | Something else |
|  :heavy_check_mark:    | Something else |


