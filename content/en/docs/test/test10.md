---
title: "Test10 - Fetch file from repo"
linkTitle: "Test10 - Fetch file from repo"
no_list: true
---

The contents of this page are pulled at build time from a .md file stored in /static/rn/. This content is pulled in as a module from a remote repo at build time This uses a custom shortcode called fetch-file.

{{% readfile "/rn/com.castsoftware.entity/2.2.md" %}}
