---
title: "Test2"
linkTitle: "Test2"
no_list: true
---

The contents of this page are pulled at build time from a .md file stored in the same repo. This uses a custom shortcode called fetch-file. The disadvantage is that URLs are badly rendered if they are simply pasted in to RNM "as is". This can be overcome by using md syntax for links: square brackets then round brackets.

{{% fetch-file "/release-notes/2.11.md" %}}