---
title: "Test4"
linkTitle: "Test4"
no_list: true
---

The contents of this page are dynamically loaded from extend.castsoftware.com when the page is built. This uses a Cloudflare Worker to achieve this. See https://quinncasey.com/hugo-remote-content-from-url/.

There are two issues currently:

1. For some reason the first heading is not rendered correctly in the HTML page
1. The consolidated markdown URL in the form https://extend.castsoftware.com/api/delta/export/release-notes/format/markdown?id=com.castsoftware.java.hibernate&major=1&minor=0 causes an error, which is due to the ? in the URL. This could probably be overcome in the worker js. Single RN URL works OK.

.

{{% fetch-worker "https://raw.githubusercontent.com/mstura/doc_test/master/markdown/com.castsoftware.dotnet/1.5.md" %}}
