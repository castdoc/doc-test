---
title: Search Results
layout: docs
---

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/instantsearch.css@7.4.5/themes/satellite-min.css">

<div class="ais-InstantSearch">
  <div class="right-panel">
    <div id="searchbox"></div>
    <div id="hits"></div>
    <div id="pagination"></div>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/algoliasearch@4.14.2/dist/algoliasearch-lite.umd.js"></script>
<script src="https://cdn.jsdelivr.net/npm/instantsearch.js@4.49.1/dist/instantsearch.production.min.js"></script>