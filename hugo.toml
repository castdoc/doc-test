baseURL = "https://doc-test.castsoftware.com"
title = "Documentation"

# Language settings
contentDir = "content/en"
defaultContentLanguage = "en"
defaultContentLanguageInSubdir = false
# Useful when translating.
enableMissingTranslationPlaceholders = true

enableRobotsTXT = true
enableEmoji = true

# Will give values to .Lastmod etc.
enableGitInfo = true

# Comment out to enable taxonomies in Docsy
disableKinds = ["RSS"]

# Highlighting config
pygmentsCodeFences = true
pygmentsUseClasses = false
# Use the new Chroma Go highlighter in Hugo.
pygmentsUseClassic = false
#pygmentsOptions = "linenos=table"
# See https://help.farbox.com/pygments.html
pygmentsStyle = "tango"

# Configure how URLs look like per section.
[permalinks]
blog = "/:section/:year/:month/:day/:slug/"

# Image processing configuration.
[imaging]
resampleFilter = "CatmullRom"
quality = 75
anchor = "smart"

[services]
[services.googleAnalytics]
# Comment out the next line to disable GA tracking. Also disables the feature described in [params.ui.feedback].
#id = "UA-00000000-0"

[markup]
  [markup.goldmark]
    [markup.goldmark.parser.attribute]
      block = true
    [markup.goldmark.renderer]
      unsafe = true
  [markup.highlight]
    # See a complete list of available styles at https://xyproto.github.io/splash/docs/all.html
    style = "tango"
    # Uncomment if you want your chosen highlight style used for code blocks without a specified language
    guessSyntax = "true"

# Everything below this are Site Params

# Comment out if you don't want the "print entire section" link enabled.
#[outputs]
#section = ["HTML", "print", "RSS"]

[params]
description = "Deep insights into your application's inner workings…"
copyright = "CAST"
privacy_policy = "https://www.castsoftware.com/privacy"

# First one is picked as the Twitter card image if not set on page.
# images = ["images/project-illustration.png"]

# Menu title if your navbar has a versions selector to access old versions of your site.
# This menu appears only if you have at least one [params.versions] set.
version_menu = "Releases"

# Flag used in the "version-banner" partial to decide whether to display a
# banner on every page indicating that this is an archived version of the docs.
# Set this flag to "true" if you want to display the banner.
archived_version = false

# The version number for the version of the docs represented in this doc set.
# Used in the "version-banner" partial to display a version number for the
# current doc set.
version = "0.0"

# A link to latest version of the docs. Used in the "version-banner" partial to
# point people to the main doc site.
url_latest_version = "https://example.com"

# Repository configuration (URLs for in-page links to opening issues and suggesting changes)
#github_repo = "https://gitlab.com/castdoc/doc/"

# An optional link to a related project repo. For example, the sibling repository where your product code lives.
#github_project_repo = "https://github.com/google/docsy"

# Specify a value here if your content directory is not in your repo's root directory
#github_subdir = "doc"

# Uncomment this if your GitHub repo does not have "main" as the default branch,
# or specify a new value if you want to reference another branch in your GitHub links
#github_branch= "main"

# Google Custom Search Engine ID. Remove or comment out to disable search.
#gcs_engine_id = "e39b3a4e4bff6475c"

# Enable Algolia DocSearch
[params.search.algolia]
appId = "VBLPVKDJJQ"
apiKey = "013b92c7b5e72d6674bab70a4e4f7edd"
indexName = "castsoftware"

# Enable Lunr.js offline search
offlineSearch = false

# Enable syntax highlighting and copy buttons on code blocks with Prism
prism_syntax_highlighting = false

# User interface configuration
[params.ui]
#  Set to true to disable breadcrumb navigation.
breadcrumb_disable = false
# Set to true to disable the About link in the site footer
footer_about_disable = false
# Set to false if you don't want to display a logo (/assets/icons/logo.svg) in the top navbar
navbar_logo = true
# Set to true if you don't want the top navbar to be translucent when over a `block/cover`, like on the homepage.
navbar_translucent_over_cover_disable = false
# Enable to show the side bar menu in its compact state.
sidebar_menu_compact = false
# Set to true to hide the sidebar search box (the top nav search box will still be displayed if search is enabled)
sidebar_search_disable = true
# Set to true to force the sidebar entries to collapse
sidebar_menu_foldable = true
# Set cache for side bar
sidebar_cache_limit = 100

# Adds a H2 section titled "Feedback" to the bottom of each doc. The responses are sent to Google Analytics as events.
# This feature depends on [services.googleAnalytics] and will be disabled if "services.googleAnalytics.id" is not set.
# If you want this feature, but occasionally need to remove the "Feedback" section from a single page,
# add "hide_feedback: true" to the page's front matter.
[params.ui.feedback]
enable = false
# The responses that the user sees after clicking "yes" (the page was helpful) or "no" (the page was not helpful).
yes = 'Glad to hear it! Please <a href="https://github.com/USERNAME/REPOSITORY/issues/new">tell us how we can improve</a>.'
no = 'Sorry to hear that. Please <a href="https://github.com/USERNAME/REPOSITORY/issues/new">tell us how we can improve</a>.'

# Adds a reading time to the top of each doc.
# If you want this feature, but occasionally need to remove the Reading time from a single page,
# add "hide_readingtime: true" to the page's front matter
[params.ui.readingtime]
enable = false

# End user relevant links. These will show up on left side of footer and in the community page if you have one.
[params.links]
[[params.links.user]]
	name ="Twitter"
	url = "https://twitter.com/SW_Intelligence"
	icon = "fab fa-twitter"
	desc = "Twitter"
[[params.links.user]]
	name ="Facebook"
	url = "https://www.facebook.com/CASTSoftwareIntelligence"
	icon = "fab fa-facebook"
	desc = "Facebook"
[[params.links.user]]
	name ="YouTube"
	url = "https://www.youtube.com/channel/UCx_BN1Mr5gyYLh71yH3xs-w"
	icon = "fab fa-youtube"
	desc = "YouTube"	
[[params.links.user]]
	name ="Linkedin"
	url = "http://www.linkedin.com/company/cast"
	icon = "fab fa-linkedin"
	desc = "Linkedin"

# Other relevant links. These will show up on right side of footer and in the community page if you have one.
[[params.links.developer]]
  name = "CAST Support"
  url = "https://help.castsoftware.com/hc/en-us"
  icon = "fa-solid fa-life-ring"
  desc = "CAST Support"
[[params.links.developer]]
  name = "Pulse - CAST Blog"
  url = "https://www.castsoftware.com/pulse"
  icon = "fa-solid fa-blog"
  desc = "Pulse - CAST Blog"

# Top nav bar config

#[[menu.main]]
#name = "Home"
#weight = 49
#pre = "<i class='fa-solid fa-house'></i>"
#url = "home"

#[[menu.main]]
#name = "Technologies"
#weight = 50
#pre = "<i class='fa-solid fa-microchip'></i>"
#url = "technologies""

[[menu.main]]
name = "CAST"
weight = 51
pre = "<i class='fa-solid fa-arrow-up-right-from-square'></i>"
url = "https://castsoftware.com/"

#[[menu.main]]
#name = "Search"
#weight = 52
#pre = "<i class='fa-solid fa-magnifying-glass'></i>"
#url = "search"

# hugo module configuration

[module]
  # uncomment line below for temporary local development of module
  # replacements = "github.com/google/docsy -> ../../docsy"
  [module.hugoVersion]
    extended = true
    min = "0.119.0"
  [[module.imports]]
    path = "github.com/google/docsy"
    disable = false
  #[[module.imports]]
  #  path="gitlab.com/castdoc/extensions"
  #[[module.imports.mounts]]
  #  source = "content/en/technologies"
  #  target = "content/technologies"
  #[[module.imports]]
  #  path="gitlab.com/castdoc/export"
  #[[module.imports.mounts]]
  #  source = "TECHNOS"
  #  target = "static/export/TECHNOS"
  #[[module.imports.mounts]]
  #  source = "TG"
  #  target = "static/export/TG"
  #[[module.imports.mounts]]
  #  source = "DOCCOM"
  #  target = "static/export/DOCCOM"
  #[[module.imports.mounts]]
  #  source = "DASHBOARDS"
  #  target = "static/export/DASHBOARDS"
  #[[module.imports.mounts]]
  #  source = "FBP"
  #  target = "static/export/FBP"
  #[[module.imports.mounts]]
  #  source = "EXTEND"
  #  target = "static/export/EXTEND"
  #[[module.imports.mounts]]
  #  source = "STORAGE"
  #  target = "static/export/STORAGE"
  #[[module.imports.mounts]]
  #  source = "SECURITY"
  #  target = "static/export/SECURITY"
  #[[module.imports.mounts]]
  #  source = "ONBRD"
  #  target = "static/export/ONBRD"
  #[[module.imports.mounts]]
  #  source = "AIPCORE"
  #  target = "static/export/AIPCORE"
  #[[module.imports.mounts]]
  #  source = "SIZING"
  #  target = "static/export/SIZING"                 
  #[[module.imports.mounts]]
  #  source = "CAST"
  #  target = "static/export/CAST"
  #[[module.imports.mounts]]
  #  source = "AIPCONSOLE"
  #  target = "static/export/AIPCONSOLE"
  #[[module.imports.mounts]]
  #  source = "IMAGING"
  #  target = "static/export/IMAGING"  
  [[module.imports]]
    path = "github.com/mstura/doc_test"
  [[module.imports.mounts]]
    source = "markdown"
    target = "content/rn"

