module gitlab.com/castdoc/doc-test

go 1.21

require (
	github.com/FortAwesome/Font-Awesome v0.0.0-20230327165841-0698449d50f2 // indirect
	github.com/google/docsy v0.8.0 // indirect
	github.com/twbs/bootstrap v5.2.3+incompatible // indirect
	gitlab.com/castdoc/export v0.0.0-20240107192451-cd1a35063f4b // indirect
)
