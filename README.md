# doc-test

This repository is a testing ground/sandbox where you can make changes without worrying too much that you may break things. Having said that, please try not to _delete_ files or make _wild_ changes as these may be being used for specific testing.

A site called `doc-test` is configured in Cloudflare Pages and uses this repository in the `Hugo` build process. You can view the results here:

[https://doc-test.castsoftware.com](https://doc-test.castsoftware.com)
